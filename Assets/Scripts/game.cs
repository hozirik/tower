﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class game : MonoBehaviour {

	public float velocity = 1.6f;
	public GameObject catisol, catisag;
	private float sol, sag, solblock, sagblock, xyeni, spawnyposition;
	private int i = 0;
	bool pressedanykey = false;
	public GameObject block;
	public GameObject[] blockclone;
	public int clonenumber = 200;
	private float piksel = 2.16f;
	private Ray ray;
	private RaycastHit raycasthit;
	public GameObject scorelabel, bestscore, gameoverlabel, resetbutton, bestscoreback, backblockbutton, backblocktext;
	public Sprite signin, signout;
	public GameObject signbutton;
	BinaryFormatter bf;
	FileStream file;
	PlayerData data;
	public GameObject coin, goldgain;
	int gold;
	int[] posorneg = {-1, 1};
	bool gameover = false;
	int backnumber;

	// Use this for initialization
	void Start () {

		PlayGamesPlatform.Activate();

		Social.localUser.Authenticate((bool success) => {

			if(success)
			{
				signbutton.GetComponent<Image>().sprite = signout;
			}else {
				signbutton.GetComponent<Image>().sprite = signin;
			}
		});

		spawnyposition = GameObject.Find("block" + i).transform.position.y;

		GameObject.Find("block" + i).GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, 0);

		blockclone = new GameObject[clonenumber];
		blockclone[0] = block;

		scorelabel.GetComponent<Text>().text = i.ToString();

		if(!File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
		{
			bf = new BinaryFormatter();
			file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

			data = new PlayerData();
			data.bestscore = 0;
			data.coin = 3000;

			bf.Serialize(file, data);
			file.Close();
		}

		bf = new BinaryFormatter();
		file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

		data = (PlayerData)bf.Deserialize(file);
		file.Close();

		gold = data.coin;

		coin.GetComponent<Text>().text = gold.ToString();

		backblocktext.GetComponent<Text>().text = (100 + (10 * i)).ToString();
	}

	public float dampTime = 0.3f;
	private Vector3 velocity3 = Vector3.zero;

	void Update () {

		Vector3 point = GetComponent<Camera>().WorldToViewportPoint(GameObject.Find("block" + i).transform.position);
		Vector3 targetpos = new Vector3(GameObject.Find("block" + i).transform.position.x, GameObject.Find("block" + i).transform.position.y + 1.9f, 
			GameObject.Find("block" + i).transform.position.z);
		Vector3 delta = targetpos - GetComponent<Camera>().ViewportToWorldPoint(new Vector3(0.575f, 0.575f, point.z));
		Vector3 destination = transform.position + delta;
		destination.x = 0;
		transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity3, dampTime);
	}

	void FixedUpdate () {

		if(i >= 2 && !gameover){
			backblockbutton.SetActive(true);
		} else {
			backblockbutton.SetActive(false);
		}

		if(Input.GetMouseButton(0) && !pressedanykey)
		{
			Vector2 ray2d = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast(ray2d, Input.mousePosition);

			if(hit.collider != null)
			{
				if(hit.collider.name == "blockclick")
				{
					pressedanykey = true;

					GameObject.Find("block" + i).GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

					if(i == 0)
					{
						sol = catisol.transform.position.x;
						sag = catisag.transform.position.x;
					} else {
						sol = GameObject.Find("block" + (i-1)).transform.position.x - (GameObject.Find("block" + (i-1)).transform.localScale.x / 2 * piksel);
						sag = GameObject.Find("block" + (i-1)).transform.position.x + (GameObject.Find("block" + (i-1)).transform.localScale.x / 2 * piksel);
					}

					solblock = GameObject.Find("block" + i).transform.position.x - (GameObject.Find("block" + i).transform.localScale.x / 2 * piksel);
					sagblock = GameObject.Find("block" + i).transform.position.x + (GameObject.Find("block" + i).transform.localScale.x / 2 * piksel);

					if((sagblock > sag) && ((sagblock - sag) > 0.05f))
					{
						xyeni = (sagblock - sag) / 2;
						GameObject.Find("block" + i).transform.localScale = new Vector3(GameObject.Find("block" + i).transform.localScale.x - xyeni, 
							GameObject.Find("block" + i).transform.localScale.y, GameObject.Find("block" + i).transform.localScale.z);
						GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x - xyeni, 
							GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
					} else if((sol > solblock) && ((sol - solblock) > 0.05f))
					{
						xyeni = (sol - solblock) / 2;
						GameObject.Find("block" + i).transform.localScale = new Vector3(GameObject.Find("block" + i).transform.localScale.x - xyeni, 
							GameObject.Find("block" + i).transform.localScale.y, GameObject.Find("block" + i).transform.localScale.z);
						GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x + xyeni, 
							GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
					} else {
						if(i == 0)
						{
							GameObject.Find("block" + i).transform.position = new Vector3(0, 
								GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
						} else {
							GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + (i-1)).transform.position.x, 
								GameObject.Find("block" + i).transform.position.y, GameObject.Find("block" + i).transform.position.z);
						}
					}

					if(GameObject.Find("block" + i).transform.localScale.x <= 0)
					{
						GameObject.Find("block" + i).transform.localScale = new Vector3(0, 0, 0);
					}

					StartCoroutine(PressedKeyChange());
				}
			}
		}

		/*
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
		*/
	}

	IEnumerator PressedKeyChange() {

		if(GameObject.Find("block" + i).transform.localScale.x <= 0)
		{
			gameover = true;

			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

			data = (PlayerData)bf.Deserialize(file);
			file.Close();

            int databestscore = data.bestscore;

			if(i > data.bestscore)
			{
				bf = new BinaryFormatter();
				file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

				data = new PlayerData();
				data.bestscore = i;
                data.coin = gold;
                bf.Serialize(file, data);
                file.Close();
			} else 
			{
				bf = new BinaryFormatter();
				file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

				data = new PlayerData();
				data.bestscore = databestscore;
				data.coin = gold;
				bf.Serialize(file, data);
				file.Close();
			}

            if (Social.localUser.authenticated)
			{
				long leaderboardscore = Convert.ToInt64(i);
				Social.ReportScore(leaderboardscore, "CgkIwvSdnogLEAIQAQ", (bool success) => {

					if(success){
						print("oldu");
					}else {
						print("olmadı");
					}
				});
			}

			bestscore.GetComponent<Text>().text = "Best: " + data.bestscore;
			bestscoreback.SetActive(true);
			gameoverlabel.SetActive(true);
			resetbutton.SetActive(true);
		} else 
		{
			i += 1;

			int a = posorneg[UnityEngine.Random.Range(0,2)];

			goldgain.GetComponent<Animator>().SetBool("goldgain", true);
			StartCoroutine(SetAnimFalse());

			gold += 10;

			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			data = (PlayerData)bf.Deserialize(file);
			file.Close();

			int databestscore = data.bestscore;

			bf = new BinaryFormatter();
			file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

			data = new PlayerData();
			data.bestscore = databestscore;
			data.coin = gold;
			bf.Serialize(file, data);
			file.Close();

			coin.GetComponent<Text>().text = gold.ToString();
			scorelabel.GetComponent<Text>().text = i.ToString();

			spawnyposition += 0.44f;

			if(a == 1)
			{
				blockclone[i] = (GameObject)Instantiate(GameObject.Find("block" + (i-1)), new Vector3(-2.8f + (GameObject.Find("block" + (i-1)).transform.localScale.x / 2 * piksel), 
					spawnyposition, 9), Quaternion.identity);
				blockclone[i].name = "block" + i;
				blockclone[i].GetComponent<Rigidbody2D>().velocity = new Vector2(velocity + (0.06f * i), 0);
			} else {
				blockclone[i] = (GameObject)Instantiate(GameObject.Find("block" + (i-1)), new Vector3(2.8f - (GameObject.Find("block" + (i-1)).transform.localScale.x / 2 * piksel), 
					spawnyposition, 9), Quaternion.identity);
				blockclone[i].name = "block" + i;
				blockclone[i].GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity - (0.06f * i), 0);
			}

			yield return new WaitForSeconds(0.2f);
			pressedanykey = false;
		}
	}

	IEnumerator SetAnimFalse() {
		yield return new WaitForSeconds(0.1f);
		goldgain.GetComponent<Animator>().SetBool("goldgain", false);
	}

	public void BackButtonPressed()
	{
		float kayma = (GameObject.Find("block" + (i-1)).transform.localScale.x - GameObject.Find("block" + i).transform.localScale.x) / 2 * piksel;

		if(GameObject.Find("block" + i).GetComponent<Rigidbody2D>().velocity.x > 0)
		{
			GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x - kayma, 
				GameObject.Find("block" + i).transform.position.y - 0.44f, GameObject.Find("block" + i).transform.position.z);
		}else {
			GameObject.Find("block" + i).transform.position = new Vector3(GameObject.Find("block" + i).transform.position.x + kayma, 
				GameObject.Find("block" + i).transform.position.y - 0.44f, GameObject.Find("block" + i).transform.position.z);
		}

		GameObject.Find("block" + i).transform.localScale = GameObject.Find("block" + (i-2)).transform.localScale;
		i -= 1;
		spawnyposition -= 0.44f;
		scorelabel.GetComponent<Text>().text = i.ToString();
		Destroy(GameObject.Find("block" + i));
		GameObject.Find("block" + (i+1)).name = "block" + (i);

		gold -= (100 + (10 * backnumber));
		coin.GetComponent<Text>().text = gold.ToString();

		backnumber += 1;
		backblocktext.GetComponent<Text>().text = (100 + (10 * backnumber)).ToString();
	}
}

[Serializable]
class PlayerData
{
	public int bestscore;
	public int coin;
}
