﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class pausebutton :MonoBehaviour , IPointerClickHandler
{
	public static bool pressed = false;
	public GameObject pausemenu, resetbutton, gameover, bestscore, bestscoreback, pauseleft, backblokbutton;
	public static bool acik = false;
	BinaryFormatter bf;
	FileStream file;
	PlayerData playerdata;

	public void OnPointerClick(PointerEventData data)
	{
		bf = new BinaryFormatter();
		file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);

		playerdata = (PlayerData)bf.Deserialize(file);
		file.Close();

		bestscore.GetComponent<Text>().text = "Best: " + playerdata.bestscore;

		backblokbutton.SetActive(false);

		if(resetbutton.activeInHierarchy)
		{
			acik = true;
			resetbutton.SetActive(false);
			gameover.SetActive(false);

		} else if(acik)
		{
			resetbutton.SetActive(true);
			gameover.SetActive(true);
		}

		if(!pressed)
		{
			bestscoreback.SetActive(true);
			//pausemenu.SetActive(true);
			pausemenu.GetComponent<Animator>().SetBool("acil", true);
			pauseleft.GetComponent<Animator>().SetBool("acil", true);
			Time.timeScale = 0;
			pressed = true;

		} else {
			if(!acik)
			{
				bestscoreback.SetActive(false);
			}
			//pausemenu.SetActive(false);
			pausemenu.GetComponent<Animator>().SetBool("kapan", true);
			pauseleft.GetComponent<Animator>().SetBool("kapan", true);
			Time.timeScale = 1;
			pressed = false;
		}

		StartCoroutine(SetFalse());
	}

	private void Update()
	{

	}

	IEnumerator SetFalse()
	{
		yield return new WaitForSeconds(0.1f);
		pausemenu.GetComponent<Animator>().SetBool("acil", false);
		pausemenu.GetComponent<Animator>().SetBool("kapan", false);
		pauseleft.GetComponent<Animator>().SetBool("acil", false);
		pauseleft.GetComponent<Animator>().SetBool("kapan", false);
	}
}
