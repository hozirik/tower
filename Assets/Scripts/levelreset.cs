﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class levelreset :MonoBehaviour , IPointerClickHandler
{
	public void OnPointerClick(PointerEventData data)
	{
		// reload the scene
		pausebutton.pressed = false;
		pausebutton.acik = false;
		SceneManager.LoadScene("Game");
		Time.timeScale = 1;
	}

	private void Update()
	{
		
	}
}
